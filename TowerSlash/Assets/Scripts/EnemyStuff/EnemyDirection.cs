﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDirection : MonoBehaviour
{
    [SerializeField] Sprite[] frame = new Sprite[2];
    [SerializeField] Sprite arrows;
    [SerializeField] SpriteRenderer frameRend, arrowRend;

    [SerializeField] float rotSpd;
    public Direction dir;
    public int points;

    public void Initialize(){
        dir = new Direction(Random.Range(0, 2));
        SetFrame(0);
        if (dir.variation < (Direction.SwipeVariation)2)
            SetArrow(dir.direction, Direction.typeSwipe[(int)dir.variation]);
        else {
            StartCoroutine("RotateArrow");
        }
    }

    IEnumerator RotateArrow()
    {
        int startingDir = Random.Range(0, 4);
        while (dir.variation == Direction.SwipeVariation.rotating) {
            startingDir = startingDir >= 3 ? 0 : startingDir + 1;
            dir.direction = (Direction.DirectionType)startingDir;
            SetArrow((Direction.DirectionType)startingDir, Direction.typeSwipe[(int)dir.variation]);
            yield return new WaitForSeconds(rotSpd);
        }
    }

    public void SetArrow(Direction.DirectionType dir, Color color) {
        arrowRend.transform.eulerAngles = new Vector3(0, 0, Direction.arrowAngle[dir]);
        arrowRend.color = color;
    }

    public void SetFrame(int i) {
        frameRend.sprite = frame[i];
        if(i == 1 && dir.variation == Direction.SwipeVariation.rotating){
            dir.variation = (Direction.SwipeVariation)0;
        }
    }

    public void CompareSwipe(TouchInput player) {
        //if (!player.didSwipeInput) return false;
        if(!(Vector2.Distance(player.transform.position, transform.position) <= player.attackingRange)) return;

        bool correctSwipe;
        if (dir.variation == Direction.SwipeVariation.opposite) { //checks whether the player swipe is opposite from enemies stored dir 
            correctSwipe = Direction.oppositeInput[player.dir] == dir.direction;
        }
        else {
            correctSwipe = player.dir == dir.direction;
        }

        if(correctSwipe) GetComponent<Health>().TakeDamage(1);
    }
}
