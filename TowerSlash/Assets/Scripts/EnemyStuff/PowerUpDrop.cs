﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDrop : MonoBehaviour {
    public PowerUp toDrop;
    
    [SerializeField] SpriteRenderer powerUpRend;
    
    public void RandomizePowerUp() {

        int random = Random.Range(0, 30) + 1;
        if (random == 1)
        {
            powerUpRend.sprite = Resources.Load<Sprite>("Sprites/Invincibility");
            toDrop = Resources.Load<PowerUp>("PowerUp/Invincibility");
        }
        else if (random <= 5)   
        {
            powerUpRend.sprite = Resources.Load<Sprite>("Sprites/Shield");
            toDrop = Resources.Load<PowerUp>("PowerUp/Shield");
        }
        else
        {
            powerUpRend.sprite = null;
            toDrop = null;
        }
    }

}
