﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyHealth : Health {

    public Action<GameObject> getScoreEvent;

    public override void Death(){
        gameObject.SetActive(false);
        getScoreEvent.Invoke(gameObject);
    }


}
