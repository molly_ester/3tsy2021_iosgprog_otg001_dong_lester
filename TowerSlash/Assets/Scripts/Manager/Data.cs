﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Data {
    public static GameObject playerPrefab;
    public static GameObject petPrefab;
    public static List<GameObject> unSelected;
    public static int highScore;

    public static void SetHighScore(int score) {
        if (score > highScore) highScore = score;
    }

}
