using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public enum States
    {
        Start, CharacterSelection, Play, GameOver
    };

    static States _state;

    public static States state
    {
        get { return _state; }
        set
        {
            _state = value;
            switch (_state)
            {
                case States.Start:
                    SceneManager.LoadScene("TitleScreen");
                    break;
                case States.Play:
                    SceneManager.LoadScene("Test");
                    break;
                case States.CharacterSelection:
                    SceneManager.LoadScene("CharacterSelection");
                    break;
            }
        }
    }

    [SerializeField] GameObject player;
    GameObject pet;
    TouchInput playerInput;
    PlayerHealth pHealth;

    [SerializeField] List<GameObject> enemy = new List<GameObject>();
    public GameObject gameOver;
    public EnemySpawner spawner;

    [SerializeField] Text displayText;
    [SerializeField] Button specialButton;
    [SerializeField] CameraController camera;

    void Awake()
    {
        player = Instantiate(Data.playerPrefab, (Vector3)new Vector2(.45f, -1.5f), Quaternion.Euler(0, 0, 90));
        pet = Instantiate(Data.petPrefab, (Vector3)new Vector2(0, 0), Quaternion.Euler(0, 0, 90));
        pet.GetComponent<PetMovement>().Initialize(player);
        pet.GetComponent<PetBuff>().ApplyBuff(player);
        player.GetComponent<PlayerScore>().displayText = displayText;
        player.GetComponent<PowerUpHandler>().specialButton = specialButton;
        camera.player = player.transform;
    }

    void Start()
    {
        playerInput = player.GetComponent<TouchInput>();
        pHealth = player.GetComponent<PlayerHealth>();
        pHealth.deathEvent += playerDeath;
        StartCoroutine(spawnEnemy());

        foreach (GameObject enemy in spawner.enemies) {
            enemy.GetComponent<EnemyHealth>().getScoreEvent += (x) => {
                player.GetComponent<PlayerScore>().AddScore(x.GetComponent<EnemyDirection>().points);
                player.GetComponent<PowerUpHandler>().StorePowerUp(x);
                this.enemy.Remove(x);
                playerInput.OnSwipe -= x.GetComponent<EnemyDirection>().CompareSwipe;
                playerInput.OnSwipe += this.enemy[0].GetComponent<EnemyDirection>().CompareSwipe;
                x.transform.parent = spawner.transform;
                spawner.enemies.Add(x);
            };
        }
    }

    void Update()
    {
        if (enemy.Count <= 0 || !player) return;

        if (Vector2.Distance(player.transform.position, enemy[0].transform.position) <= playerInput.attackingRange) {
            enemy[0].GetComponent<EnemyDirection>().SetFrame(1);
        }
    }

    void playerDeath()
    {
        player = null;
        gameOver.SetActive(true);
        StopCoroutine(spawnEnemy());
        if(Time.timeScale < 1) Time.timeScale = 1;
    }

    IEnumerator spawnEnemy(){
        while (player) {
            yield return new WaitForSeconds(Random.Range(spawner.minSecsForSpawn, spawner.maxSecsForSpawn));
            GameObject spawn = spawner.SpawnEnemy();
            if (enemy.Count < 1) playerInput.OnSwipe += spawn.GetComponent<EnemyDirection>().CompareSwipe;
            enemy.Add(spawn);
            
        }
    }

    public void Retry()
    {
        state = States.Play;
    }

    public void BackToMainMenu()
    {
        state = States.Start;
    }

    public void CharacterSelection()
    {
        state = States.CharacterSelection;
    }
}