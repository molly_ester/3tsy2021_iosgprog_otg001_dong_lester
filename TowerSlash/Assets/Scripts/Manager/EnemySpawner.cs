﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public List<GameObject> enemies = new List<GameObject>();
    public float minSecsForSpawn, maxSecsForSpawn;

    void Start() {
        //foreach(GameObject enemy in transform){
        for (int i = 0; i < enemies.Count; i++){
            enemies[i].SetActive(false);
        }
    }

    void Update() {
        Vector2 obj = Camera.main.WorldToViewportPoint(transform.position);
        obj.y = 1.5f;
        transform.position = Camera.main.ViewportToWorldPoint(obj);
    }

    public GameObject SpawnEnemy() {
        int toSpawn = Random.Range(0, enemies.Count);
        GameObject enemyToSpawn = enemies[toSpawn];
        //Enemies enemy = Instantiate(prefabs[toSpawn], transform.position, Quaternion.identity).GetComponent<Enemies>();

        enemyToSpawn.transform.parent = null;
        enemyToSpawn.transform.position = transform.position;
        enemyToSpawn.gameObject.SetActive(true);
        enemyToSpawn.GetComponent<Health>().Replenish();
        enemyToSpawn.GetComponent<EnemyDirection>().Initialize();
        enemyToSpawn.GetComponent<PowerUpDrop>().RandomizePowerUp();
        enemies.Remove(enemyToSpawn);
        return enemyToSpawn;
    }

    //EnemyHealth
    //EnemyDirection
    //PowerUpDrop
}