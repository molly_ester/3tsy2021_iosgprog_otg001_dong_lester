﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour
{
    float size;
    SpriteRenderer[] tiles = new SpriteRenderer[2];
    int currentVisible = 0;

    void Start(){
        
        for (int i = 0; i < transform.childCount;i++){
            tiles[i] = transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>();
        }
        size = 16;
    }

    void Update(){
        /*if (!tiles[currentVisible].isVisible){
            tiles[currentVisible].transform.position += new Vector3(0, size * 2, 0);
            currentVisible = currentVisible == 0 ? currentVisible + 1 : 0;
            Debug.Log(currentVisible);
        }*/
        //Debug.Log(tiles[currentVisible].isVisible); 
        if (isVisible(currentVisible)){
            tiles[currentVisible].transform.position += new Vector3(0, size * 2, 0);
            currentVisible = currentVisible == 0 ? currentVisible + 1 : 0;
        }

    }

    bool isVisible(int cur){
        Vector2 obj = Camera.main.WorldToViewportPoint(tiles[cur].transform.position);
        return obj.y <= -1.0;
    }
}
