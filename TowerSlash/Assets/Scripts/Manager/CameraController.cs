﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    [SerializeField] Vector3 offset;
    [SerializeField] float spd;
    SpriteRenderer bg;

    void Start(){
        Camera.main.transform.position = player.position + new Vector3(0, 0, -10);
        Camera.main.transform.position = player.position + offset;
    }
    
    void Update()
    {
        if (player){
            Camera.main.transform.position = new Vector3(0, Mathf.Lerp(transform.position.y, player.position.y + offset.y, spd * Time.deltaTime), offset.z); //player.position + offset;
        }
    }

    public void SetUp(){
        Camera.main.transform.position = player.position + new Vector3(0, 0, -10);
        Camera.main.transform.position = player.position + offset;
    }
}
