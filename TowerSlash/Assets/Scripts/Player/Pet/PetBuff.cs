﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetBuff : MonoBehaviour {
    
    [SerializeField] protected float buffPer;
    
    public virtual void ApplyBuff(GameObject player) {

    }

    public virtual void RemoveBuff(GameObject player) {

    }
}