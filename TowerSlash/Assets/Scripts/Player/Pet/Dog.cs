﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : PetBuff {
    public override void ApplyBuff(GameObject player) {
        player.GetComponent<Movement>().spdBoost += buffPer;
    }

    public override void RemoveBuff(GameObject player)
    {
        player.GetComponent<Movement>().spdBoost -= buffPer;
    }
}
