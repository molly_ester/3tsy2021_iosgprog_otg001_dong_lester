﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : PetBuff {
    public override void ApplyBuff(GameObject player) {
        player.GetComponent<TouchInput>().attackingRange += buffPer;
        GetComponent<SpriteRenderer>().flipX = true;
    }

    public override void RemoveBuff(GameObject player) {
        player.GetComponent<TouchInput>().attackingRange -= buffPer;
    }
}
