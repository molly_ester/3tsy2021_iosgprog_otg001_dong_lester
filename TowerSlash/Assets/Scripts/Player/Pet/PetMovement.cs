﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetMovement : MonoBehaviour {
    public Vector3 offset;

    Movement follow;
    public void Initialize(GameObject obj){
        transform.position = obj.transform.position - offset;
        follow = obj.GetComponent<Movement>();
    }

    void Update(){
        transform.Translate(follow.CalculatedSpeed * Time.deltaTime, 0, 0);
    }
}
