﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hamster : PetBuff {

    public override void ApplyBuff(GameObject player) {
        player.GetComponent<PlayerScore>().multiplier += buffPer;
    }

    public override void RemoveBuff(GameObject player)
    {
        player.GetComponent<PlayerScore>().multiplier -= buffPer;
    }
}
