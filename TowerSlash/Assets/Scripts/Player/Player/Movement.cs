﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour { 

    [SerializeField] float spd;
    [SerializeField] float dashSpeed;
    public float spdBoost;
    public bool isDashing;

    void Update(){
        transform.Translate(new Vector3(CalculatedSpeed * Time.deltaTime, 0, 0));
    }

    public float CalculatedSpeed {
        get { return ((isDashing ? dashSpeed : spd) + spdBoost); }
    }
}