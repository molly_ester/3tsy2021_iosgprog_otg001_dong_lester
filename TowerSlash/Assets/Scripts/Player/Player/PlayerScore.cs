﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    public int score;
    public float multiplier;

    Movement movement;
    public Text displayText;

    void Start(){
        movement = GetComponent<Movement>();
    }

    void Update(){
        AddScore((int) Mathf.Floor(movement.CalculatedSpeed / 2));
        displayText.text = score.ToString();
    }

    public void AddScore(int score){
        this.score += (int) (score * multiplier);
    }
}
