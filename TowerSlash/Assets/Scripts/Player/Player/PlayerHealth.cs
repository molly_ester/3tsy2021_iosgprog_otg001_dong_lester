﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health {

    PowerUpHandler pHandler;
    protected override void Start(){
        pHandler = GetComponent<PowerUpHandler>();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(!other.gameObject.CompareTag("Enemy")) return;
        
        if (pHandler.isInvincible){
            other.gameObject.GetComponent<Health>().TakeDamage(1);
            return;
        }
        
        else if(pHandler.isShielded){
            pHandler.powerUp.forceEnd(gameObject);
            other.gameObject.GetComponent<Health>().TakeDamage(1);
            return;
        }

        TakeDamage(1);
    }
}
