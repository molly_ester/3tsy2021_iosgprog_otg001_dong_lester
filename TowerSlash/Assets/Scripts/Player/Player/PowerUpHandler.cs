﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpHandler : MonoBehaviour {

    PowerUp _powerUp;
    public bool isInvincible;
    public bool isShielded;

    [SerializeField] SpecialPowerUp sPower;
    public int totalKills;
    SpecialPowerUp forUse;

    public Button specialButton;

    void Start(){
        specialButton.gameObject.SetActive(false);
        specialButton.onClick.AddListener(() => ActivateSpecial());
    }

    public PowerUp powerUp {
        get { return _powerUp; }
        set {
            _powerUp = value;
            if (_powerUp == null) {
                if (isInvincible) 
                    isInvincible = false;
                else 
                    isShielded = false;
                return;
            }
            if (_powerUp is Invincibility)
                isInvincible = true;
            else
                isShielded = true;

            StartCoroutine(_powerUp.TakeEffect(gameObject));
        }
    }


    public void StorePowerUp(GameObject enemy){
        if(!ChargeSpecial(enemy)) return;
        if(!enemy.GetComponent<PowerUpDrop>().toDrop || powerUp != null) return;

        powerUp = Instantiate(enemy.GetComponent<PowerUpDrop>().toDrop);
    }

    public void ActivateSpecial(){
        powerUp?.forceEnd(gameObject);
        powerUp = null;
        forUse = Instantiate(sPower);
        StartCoroutine(forUse.TakeEffect(gameObject));
        totalKills = 0;
        specialButton.gameObject.SetActive(false);
    }

    public bool ChargeSpecial(GameObject obj){
        if(forUse) return false;
        totalKills++;
        if (sPower.killsNeeded <= totalKills)
        {
            specialButton.gameObject.SetActive(true);
        }
        return true;
    }
    
}
