﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TouchInput : MonoBehaviour {
    
    Movement movement;
    float swipeThreshold = .15f;

    Vector2 startSwipePos, endSwipePos;

    public float attackingRange;
    public Action<TouchInput> OnSwipe;

    public Direction.DirectionType dir {
        get { return Direction.GetSwipeDirection(startSwipePos, endSwipePos); }
    }

    void Start() {
        movement = GetComponent<Movement>();
    }

    void Update() {
        #if UNITY_EDITOR
            MouseInput();
        #elif UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
            Touch();
        #endif
    }

    public void MouseInput() {

        if (Input.GetMouseButtonDown(0)) {
            startSwipePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0)) {
        
        }
        else if (Input.GetMouseButtonUp(0)) {
            endSwipePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            if (Vector2.Distance(startSwipePos, endSwipePos) >= swipeThreshold) {
                OnSwipe.Invoke(this);
            }
        }


        switch (Input.GetMouseButton(0))
        {
            case true:
                movement.isDashing = true;
                break;
            case false:
                movement.isDashing = false;
                break;
        }
    }

    public void Touch() {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase) {
                case TouchPhase.Began:
                    startSwipePos = Camera.main.ScreenToViewportPoint(touch.position);
                    break;
                case TouchPhase.Stationary:
                    movement.isDashing = true;
                    break;
                case TouchPhase.Moved: //if fingers move, stop dash
                    movement.isDashing = false;
                    break;
                case TouchPhase.Ended:
                    movement.isDashing = false;
                    endSwipePos = Camera.main.ScreenToViewportPoint(touch.position);
                    if (Vector2.Distance(startSwipePos, endSwipePos) >= swipeThreshold)
                        OnSwipe.Invoke(this);
                    break;
            }
        }
    }
}
