﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Direction
{
    public enum DirectionType {
        up = 0, left = 1, down = 2, right = 3
    };

    public enum SwipeVariation {
        normal = 0, opposite = 1, rotating = 2
    };

    public DirectionType direction;
    public SwipeVariation variation;


    public static Dictionary<DirectionType, DirectionType> oppositeInput = new Dictionary<DirectionType, DirectionType>{
        {DirectionType.down, DirectionType.up},
        {DirectionType.up, DirectionType.down},
        {DirectionType.right, DirectionType.left},
        {DirectionType.left, DirectionType.right}
    };

    public static Dictionary<int, Color> typeSwipe = new Dictionary<int, Color> {
        {0, Color.green},
        {1, Color.red},
        {2, Color.green}
    };

    public static Dictionary<DirectionType, int> arrowAngle = new Dictionary<DirectionType, int>{
        {DirectionType.right, 0},
        {DirectionType.up, 90},
        {DirectionType.left, 180},
        {DirectionType.down, 270}

    };


    // parameter identifies whether the type of variation
    // normal and opposite will always be randomized, while rotating
    // is only randomized at start
    public Direction(int var){
        variation = (SwipeVariation) var;
        direction = (DirectionType) UnityEngine.Random.Range(0, 4);
    }

    public static DirectionType GetSwipeDirection(Vector2 startPos, Vector2 endPos){
        Vector2 mag = endPos - startPos; //direction
        mag.Normalize();

        float angle = Mathf.Atan2(mag.y, mag.x);
        angle *= 180 / Mathf.PI;
        if (angle < 0) angle = angle + 360; // make angle positive

        if (angle > 225 && angle <= 315) return DirectionType.down;
        else if (angle > 45 && angle <= 135) return DirectionType.up;
        else if (angle > 135 && angle <= 225) return DirectionType.left;
        else return DirectionType.right;
    }
}
