﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Health : MonoBehaviour {

    [SerializeField] int health = 1;
    public Action deathEvent;

    protected virtual void Start(){

    }

    public void TakeDamage(int dmg){
        health -= dmg;
        if(health <= 0){
            health = 0;
            Death();
        }
    }

    public virtual void Death() {
        deathEvent.Invoke();
        gameObject.SetActive(false);
    }

    public virtual void Replenish(){
        health = 1;
    }
}
