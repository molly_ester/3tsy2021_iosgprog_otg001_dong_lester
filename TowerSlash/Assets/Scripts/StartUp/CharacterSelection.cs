﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour {

    [SerializeField] PrefabHolder[] characters;
    Vector2 start, end;
    int currentSel = 1;

    void Start(){
        characters[currentSel].obj.SetActive(true);
    }


    void Update(){
        if(Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);

            switch(touch.phase){
                case TouchPhase.Began:
                    start = Camera.main.ScreenToViewportPoint(touch.position);
                    break;
                case TouchPhase.Ended:
                    end = Camera.main.ScreenToViewportPoint(touch.position);
                    Swipe();
                    break;
            }
        }
    }

    void Swipe(){
        Direction.DirectionType dir = Direction.GetSwipeDirection(start, end);
        if (dir == Direction.DirectionType.left) ChangeSel(1);
        else if (dir == Direction.DirectionType.right) ChangeSel(-1);
    }

    public void ChangeSel(int i){
        characters[currentSel].obj.SetActive(false);
        currentSel += i;
        currentSel = currentSel > 2 ? 0 : currentSel < 0 ? 2 : currentSel;
        characters[currentSel].obj.SetActive(true);
    }

    public void Select(){
        if (characters[currentSel].type == PrefabHolder.HolderType.Player)
            Data.playerPrefab = characters[currentSel].prefabHolder;
        else {
            Data.petPrefab = characters[currentSel].prefabHolder;
            GameManager.state = GameManager.States.Play;
            Data.unSelected = new List<GameObject>();
            for (int i = 0; i < characters.Length; i++){
                if (currentSel != i)
                {
                    Data.unSelected.Add(characters[i].prefabHolder);
                }
                
            }
        }
    }

    public void ChangeSelection(GameObject obj){
        gameObject.SetActive(false);
        obj.SetActive(true);
    }

    [Serializable]
    class PrefabHolder {
        public GameObject obj;
        public GameObject prefabHolder;
        public HolderType type;

        public PrefabHolder(GameObject placeHolder, GameObject prefab){
            obj = placeHolder;
            prefabHolder = prefab;
        }

        public enum HolderType
        {
            Player, Pet
        };
    }
}
