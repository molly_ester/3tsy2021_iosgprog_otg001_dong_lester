﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    
    void Update() {
        if (GameManager.state == GameManager.States.Start) {
            if (Input.touchCount > 0 || Input.GetMouseButtonDown(0)) {
                GameManager.state = GameManager.States.CharacterSelection;
            }
        }
    }
}
