﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SpecialPowerUp/Mage")]
public class MageSpecial : SpecialPowerUp
{
    protected override void Activate(GameObject player)
    {
        base.Activate(player);
        Time.timeScale = .5f;
    }

    protected override void End(GameObject player)
    {
        Time.timeScale = 1;
        base.End(player);
    }
}