﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SpecialPowerUp/Hunter")]
public class HunterSpecial : SpecialPowerUp {   
    List<GameObject> unselected = new List<GameObject>();

    protected override void Activate(GameObject player) {
        base.Activate(player);
        for (int i = 0; i < Data.unSelected.Count; i++)
        {
            unselected.Add(Instantiate(Data.unSelected[i], player.transform.position, Quaternion.Euler(0, 0, 90)));
            unselected[i].GetComponent<PetMovement>().offset.y = 1 + ((i + 1) / 2f);
            unselected[i].GetComponent<PetMovement>().Initialize(player);
            unselected[i].GetComponent<PetBuff>().ApplyBuff(player);
            Debug.Log(unselected[i]);
        }
    }

    protected override void End(GameObject player) {
        GameObject toDestroy;
        Debug.Log(unselected.Count);
        for (int i = 0; i < unselected.Count; i++) {
            unselected[i].GetComponent<PetBuff>().RemoveBuff(player);
            Destroy(unselected[i]);
        }
        base.End(player);
    }
}

