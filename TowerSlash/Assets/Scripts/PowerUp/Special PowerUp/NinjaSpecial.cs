﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SpecialPowerUp/Ninja")]
public class NinjaSpecial : SpecialPowerUp
{

    [SerializeField] float spdBoost;
    [SerializeField] float multiplier;
    protected override void Activate(GameObject player)
    {
        player.GetComponent<PowerUpHandler>().isInvincible = true;
        player.GetComponent<Movement>().spdBoost += spdBoost;
        player.GetComponent<PlayerScore>().multiplier += multiplier;
        base.Activate(player);
    }

    protected override void End(GameObject player)
    {
        player.GetComponent<PowerUpHandler>().isInvincible = false;
        player.GetComponent<Movement>().spdBoost -= spdBoost;
        player.GetComponent<PlayerScore>().multiplier -= multiplier;
        base.End(player);
    }
}
