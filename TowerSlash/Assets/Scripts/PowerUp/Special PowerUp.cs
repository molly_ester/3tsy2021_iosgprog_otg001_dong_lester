﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialPowerUp : PowerUp {
    public bool isSkillActive;
    public int killsNeeded;

    protected override void Activate(GameObject player){
        player.GetComponent<SpriteRenderer>().color = Color.red;
        isSkillActive = true;
    }

    protected override void End(GameObject player){
        player.GetComponent<SpriteRenderer>().color = Color.white;
        isSkillActive = false;
        base.End(player);
    }
}



