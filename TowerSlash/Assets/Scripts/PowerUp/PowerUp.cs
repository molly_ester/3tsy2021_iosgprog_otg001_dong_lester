﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : ScriptableObject{
    
    [SerializeField] float duration;

    protected virtual void Activate(GameObject player)
    {

    }

    protected virtual void End(GameObject player) {
        player.GetComponent<PowerUpHandler>().powerUp = null;
        Destroy(this);
    }

    public IEnumerator TakeEffect(GameObject player){
        Activate(player);
        yield return new WaitForSeconds(duration);
        End(player);
    }

    public void forceEnd(GameObject player){
        End(player);
    }
}
