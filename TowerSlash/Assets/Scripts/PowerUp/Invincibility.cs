﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "PowerUp/Invincibility")]
public class Invincibility : PowerUp
{
    public float spdMult;
    Movement m;
    
    protected override void Activate(GameObject player) {
        m = player.GetComponent<Movement>();
        m.spdBoost += 2.5f * spdMult;
    }

    protected override void End(GameObject player){
        m.spdBoost -= 2.5f * spdMult;
        base.End(player);
    }
}
