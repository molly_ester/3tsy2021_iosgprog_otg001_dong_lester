﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PowerUp/Shield")]
public class Shield : PowerUp {
    
    protected override void Activate(GameObject player) {
        player.transform.GetChild(0).gameObject.SetActive(true);
    }

    protected override void End(GameObject player) {
        player.transform.GetChild(0).gameObject.SetActive(false);
        base.End(player);
    }
}
    