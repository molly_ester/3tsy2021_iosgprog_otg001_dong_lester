﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public Transform Target;
    public Vector2 minCameraBound, maxCameraBound;
    Vector3 offset = new Vector3(0, 0, -10);

    void Start(){
        
        GameManager.Instance.EvtGameStart += () => Target = GameManager.Instance.Player.transform;
    }

    void Update(){
        if(!Target) return;
        //transform.position = target.position + offset;
        if(Target.position != transform.position){
            Vector3 pos = Target.position + offset;
            pos.x = Mathf.Clamp(pos.x, minCameraBound.x, maxCameraBound.x);
            pos.y = Mathf.Clamp(pos.y, minCameraBound.y, maxCameraBound.y); 
            transform.position = pos;   
        } 
    }

    public static bool IsObjectOutOfBounds(GameObject obj){
        return true;
    }
}
