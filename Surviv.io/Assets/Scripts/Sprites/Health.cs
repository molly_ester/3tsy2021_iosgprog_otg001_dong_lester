﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EvtHealth : UnityEvent<Health> { }
public class Health : MonoBehaviour {
    
    int curValue = 100, maxValue = 100;

    public float CurHealth => curValue;
    public float MaxHealth => maxValue;
    public bool IsDead => curValue <= 0;
    public float Ratio => CurHealth / MaxHealth;
    

    public EvtHealth EvtChangeHealth {get; private set;}
    public EvtHealth EvtDeath {get; private set;}

    void Awake(){
        EvtChangeHealth = new EvtHealth();
        EvtDeath = new EvtHealth();
    }

    public void TakeDamage(int damage){
        if (damage <= 0) return;

        curValue -= damage;
        curValue = Mathf.Max(curValue, 0);
        EvtChangeHealth.Invoke(this);
        if(IsDead)
            Death();
    }

    public void Heal(int heal){
        if (heal <= 0 || IsDead) return;
        
        curValue += heal;
        curValue = Mathf.Min(curValue, maxValue);
        EvtChangeHealth.Invoke(this);
    }

    public void Death(){

        EvtDeath.Invoke(this);
    }
}
