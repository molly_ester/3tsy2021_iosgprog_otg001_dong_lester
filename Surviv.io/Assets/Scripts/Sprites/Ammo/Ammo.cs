﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ammo")]
public class Ammo : ScriptableObject, IGunType {
    [SerializeField] Sprite sprite;
    int amount;
    [SerializeField] AmmoType type;
    public string Name => this.name;

    public Sprite Sprite => sprite;
    public int Amount => amount;
    public AmmoType AmmoType => type;

    public void AddAmmo(int value){
        amount += value;
    }

    public void ReduceAmmo(int value){
        amount -= value;
        amount = Mathf.Max(amount, 0 );
    }
    
}

public enum AmmoType{
    pistol, shotgun, auto
};