﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit{

    public VirtualJoystick moveJoystick;
    public VirtualJoystick aimJoyStick;
    public ShootButton shootButton;

    void Update(){
        if(!moveJoystick && !aimJoyStick) return;   
        Movement.Move(moveJoystick.InputVector);

        if(aimJoyStick.InputVector != Vector2.zero){
            float rotation = Mathf.Atan2(aimJoyStick.InputVector.y, aimJoyStick.InputVector.x) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, 0, rotation);
            WeaponHandler.Direction = aimJoyStick.InputVector;
        }
    }
}