﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
    [SerializeField] float speed;

    public float Speed => speed;
    public void Move(Vector2 direction){
        transform.position += (Vector3) direction * speed * Time.deltaTime;
    } 

    public bool MoveToPosition(Vector2 position){
        Vector3 dir = (Vector3) position - transform.position;
        if(Vector2.Distance(transform.position, position) > .5f){
            Move(dir.normalized);
            return false;
        }
        return true;
    }
}
