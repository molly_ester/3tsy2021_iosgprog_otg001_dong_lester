﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gun/Pistol")]
public class Pistol : Gun {

    public override void Shoot(Transform shooter){
        BulletInitialization(shooter.position, shooter.gameObject.GetComponent<WeaponHandler>().Direction.normalized);
        base.Shoot(shooter);
    }
}
