﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : ScriptableObject, IGunType {

    [SerializeField] protected Sprite sprite;
    [SerializeField] float shootSpeed;
    [SerializeField] float reloadSpeed;
    [SerializeField] protected AmmoType ammoType;

    [Space]

    [SerializeField] protected int maxAmmo;
    [SerializeField] int ammo = 0;
    [SerializeField] protected int damage;
    
    public bool hasAmmo => ammo > 0;
    public int Ammo => ammo;
    public Sprite Sprite => sprite;
    public AmmoType AmmoType => ammoType;
    public float ShootSpeed => shootSpeed;
    public float ReloadSpeed => reloadSpeed;

    public virtual void Shoot(Transform shooter){
        ammo--;
    }

    public void Reload(Ammo ammo){
        int toAddAmmo = maxAmmo - this.ammo;
        toAddAmmo = ammo.Amount < maxAmmo ? ammo.Amount : toAddAmmo; 
        ammo.ReduceAmmo(toAddAmmo);
        this.ammo += toAddAmmo;
    } 

    protected void BulletInitialization(Vector2 startingPos, Vector2 dir){
        Projectile projectileObj = PoolingManager.Instance.GetProjectile(startingPos + dir);
        projectileObj.Initialize(dir * 10.0f);
        projectileObj.Damage = damage;
    }
}

public interface IGunType {
    Sprite Sprite {get;}
    AmmoType AmmoType {get;}
}