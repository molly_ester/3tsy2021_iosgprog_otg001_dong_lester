﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gun/Shotgun")]
public class Shotgun : Gun {
    [SerializeField] int numOfBullets;
    
    public override void Shoot(Transform shooter){
        for(int i = 0; i < numOfBullets; i++){
            BulletInitialization(shooter.position, shooter.gameObject.GetComponent<WeaponHandler>().Direction.normalized + new Vector2(Random.Range(-.5f, .5f)/3, Random.Range(-.5f, .5f)/3).normalized);
        }
        
        base.Shoot(shooter);
    }
}
