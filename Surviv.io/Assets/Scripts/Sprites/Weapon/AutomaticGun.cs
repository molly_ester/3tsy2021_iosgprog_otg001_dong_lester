﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gun/AutomaticGun")]
public class AutomaticGun : Gun {

    public override void Shoot(Transform shooter){
        BulletInitialization(shooter.position, (shooter.gameObject.GetComponent<WeaponHandler>().Direction.normalized +  new Vector2(Random.Range(-.5f, .5f)/3, Random.Range(-.5f, .5f)/3)).normalized);
        
    }
}
