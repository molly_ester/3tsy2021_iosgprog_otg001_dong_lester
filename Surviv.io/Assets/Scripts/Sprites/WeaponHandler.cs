﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class HandlerEvt : UnityEvent<WeaponHandler> { }

public class WeaponHandler : MonoBehaviour {

#region Initialization
    [SerializeField] Gun[] guns = new Gun[2];
    [SerializeField] Ammo[] ammos = new Ammo[3];
    [SerializeField] SpriteRenderer gunRend;
    [SerializeField] bool canSwap;

    int curSel;

    bool canShoot;
    bool isReloading;

    public Gun CurEquippedGun => guns[curSel];
    public Ammo[] Ammos => ammos;
    public bool HasPrimaryGun => guns[0];
    public bool HasSecondaryGun => guns[1];

    public HandlerEvt EvtAmmoChange {get; private set;}
    public HandlerEvt EvtNewWeapon {get; private set;}
    public Vector2 Direction {get;set;}
    
    void Awake() {
        EvtAmmoChange = new HandlerEvt();
        EvtNewWeapon = new HandlerEvt();
    }

    void Start() {
        Direction = Vector2.right;
        for(int i = 0; i < ammos.Length; i++){
            ammos[i] = Instantiate(ammos[i]);
        }
    }

    void UpdateGunSprite(){
        gunRend.sprite = CurEquippedGun.Sprite;
    }

#endregion

#region GunCommands

    public bool AddGun(Gun gun){
        bool didTake = false;
        if((gun.AmmoType == AmmoType.shotgun || gun.AmmoType == AmmoType.auto)){
            didTake = canSwap || !guns[0];
            didTake = guns[0] ? didTake && gun.AmmoType != guns[0].AmmoType : didTake;
            if(didTake && guns[0]) Destroy(guns[0]);
            guns[0] = didTake ? gun : guns[0];
        }
        else if (gun.AmmoType == AmmoType.pistol && !guns[1]) {
            guns[1] = gun;
            didTake = true;
        } else {
            return didTake;
        }
        

        if(!CurEquippedGun){
            NextGun();
        }
        else{
            UpdateGunSprite();
            EvtAmmoChange.Invoke(this);
            Reload();
        }

        EvtNewWeapon.Invoke(this);

        return didTake;
    }

    public bool AddAmmo(Ammo ammo){
        for(int i = 0; i < ammos.Length; i++){
            if(ammo.AmmoType == ammos[i].AmmoType){
                ammos[i].AddAmmo(ammo.Amount);
                EvtAmmoChange.Invoke(this);
                if(CurEquippedGun){
                    if(!CurEquippedGun.hasAmmo){
                        Reload();
                    }
                }
                return true;
            }
        }
        return false;
    }

    public void NextGun(){
        if(!HasNextGun(out int nextSel)) return;

        StopAllCoroutines();
        canShoot = true;
        isReloading = false;

        curSel = nextSel;

        EvtAmmoChange.Invoke(this);

        if(!CurEquippedGun.hasAmmo){
            Reload();
        }

        UpdateGunSprite();
    }

    public Ammo CurMagazine(Gun gun) {
        for(int i = 0; i < ammos.Length; i++){
            if(gun.AmmoType == ammos[i].AmmoType){
                return ammos[i];
            }
        }
        return null;
    }

    public Gun GetNextGun(){
        if(!HasNextGun(out int next)) return null;
        
        return guns[next];
    }

    public Ammo CurMagazine() {
        if(!CurEquippedGun) return ammos[0];
        for(int i = 0; i < ammos.Length; i++){
            if(CurEquippedGun.AmmoType == ammos[i].AmmoType){
                return ammos[i];
            }
        }
        return null;
    }
#endregion

#region Combat
  public void Attack(){
        if(!CurEquippedGun) return;
        if(canShoot && CurEquippedGun.hasAmmo) {
            CurEquippedGun.Shoot(transform);
            canShoot = false;
            StartCoroutine(ShootCoolDown()); 
            EvtAmmoChange.Invoke(this);
        }

        if(!CurEquippedGun.hasAmmo && !isReloading){
            Reload();
        }
    }

    public void HoldableAttack(){
        if(!(CurEquippedGun is  AutomaticGun)){
            return;
        }
        
        Attack();
    }

    public void Reload(){
        if(CanReload(out Ammo ammo)){
            StartCoroutine(ReloadTime(ammo));
        }
    }

    public IEnumerator ShootCoolDown(){
        yield return new WaitForSeconds(CurEquippedGun.ShootSpeed);
        canShoot = true;
    }

    
    IEnumerator ReloadTime(Ammo ammo){
        canShoot = false;
        isReloading = true;
        yield return new WaitForSeconds(CurEquippedGun.ReloadSpeed);
        CurEquippedGun.Reload(ammo);
        isReloading = false;
        canShoot = true;
        EvtAmmoChange.Invoke(this);
    }

#endregion

#region Checkers
    public bool CanReload(out Ammo ammo){
        ammo = CurMagazine();
        return ammo.Amount > 0;
    }

    bool HasNextGun(out int next){
        next = curSel + 1;
        next = next >= guns.Length ? 0 : next;
        next = next < 0 ? guns.Length : next; 
        return guns[next];
    }

    public bool HasNextGun() {
        return HasNextGun(out int x);
    }

    public bool CanReload(Gun gun){
        Ammo ammo  = CurMagazine(gun);
        return ammo.Amount > 0;
    }

    public bool CanReload(){
        Ammo ammo = CurMagazine();
        return ammo.Amount > 0;
    }
#endregion

    void OnDestroy() {
        for(int i = 0; i < guns.Length; i++){
            if(guns[i]){
                Destroy(guns[i]);
            }
        }

        for(int i = 0; i < ammos.Length; i++){
            if(ammos[i]){
                Destroy(ammos[i]);
            }
        }
    }
}
