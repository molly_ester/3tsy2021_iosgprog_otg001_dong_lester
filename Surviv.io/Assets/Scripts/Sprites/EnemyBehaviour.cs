﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : StateMachine {
    
    public Enemy Enemy;

    void Start(){
        Enemy = GetComponent<Enemy>();
        SetState(new Idle(this));

        Enemy.EvtOnEnemyDetection.AddListener(() => {
            if(Enemy.WeaponHandler.CurEquippedGun){
                StopAllCoroutines();
                SetState(new Combat(this));
            }
        });

        Enemy.EvtOnContainerDetection.AddListener(() =>{
            if(state is Idle || state is Patrol){
                StopAllCoroutines();
                SetState(new Looting(this));
            }
        });
    }

}
