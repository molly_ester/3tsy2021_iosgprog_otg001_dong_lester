﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : Unit {
    
    public List<Unit> OnEnemyRange {get; private set;}
    public List<Container> OnContainerRange {get; private set;}

    public UnityEvent EvtOnEnemyDetection {get; private set;}
    public UnityEvent EvtOnContainerDetection {get; private set;}

    protected override void Awake(){
        base.Awake();
        OnEnemyRange = new List<Unit>();
        OnContainerRange = new List<Container>();
        EvtOnEnemyDetection = new UnityEvent();
        EvtOnContainerDetection = new UnityEvent();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("Unit")){
            Unit unit = other.GetComponent<Unit>();
            if (unit.Health.IsDead) 
                return;
            
            OnEnemyRange.Add(unit);   
            unit.Health.EvtDeath.AddListener((health) => OnEnemyRange.Remove(health.GetComponent<Unit>()));
            EvtOnEnemyDetection.Invoke();
        }

        if(other.gameObject.CompareTag("Loot")){
            Container cont = other.GetComponent<Container>();
            OnContainerRange.Add(cont);
            cont.EvtTaken.AddListener((contParam) => OnContainerRange.Remove(contParam));
            EvtOnContainerDetection.Invoke();
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.CompareTag("Unit")){
            Unit unit = other.GetComponent<Unit>();
            OnEnemyRange.Remove(other.GetComponent<Unit>());   
            //unit.Health.EvtDeath.RemoveListener((health) => OnEnemyRange.Remove(health.GetComponent<Unit>()));
        }

        if(other.gameObject.CompareTag("Loot")){
            Container cont = other.GetComponent<Container>();
            OnContainerRange.Remove(cont);
            //cont.EvtTaken.RemoveListener((contParam) => OnContainerRange.Remove(contParam));
        }
    }
}
