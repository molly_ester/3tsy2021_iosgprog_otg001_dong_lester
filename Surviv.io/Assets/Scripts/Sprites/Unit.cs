﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    public Movement Movement {get; protected set;}
    public Health Health {get; protected set;}
    public WeaponHandler WeaponHandler {get; protected set;}

    protected virtual void Awake(){
        Movement = GetComponent<Movement>();
        Health = GetComponent<Health>();
        WeaponHandler = GetComponent<WeaponHandler>();
    }

    protected virtual void Start(){
        Health.EvtDeath.AddListener((health) => enabled = false);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("Loot")){
            Container cont = other.gameObject.GetComponent<Container>();
            if(cont.Loot() is Gun){
                cont.IsTaken = WeaponHandler.AddGun(cont.Loot() as Gun);
            }
            if(cont.Loot() is Ammo){
                cont.IsTaken = WeaponHandler.AddAmmo(cont.Loot() as Ammo);
            }

            if(cont.IsTaken){
                Destroy(other.gameObject);
            }
        }
    }
}
