﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingManager : Singleton<PoolingManager> {

    public GameObject ProjectilePrefab;
    public List<Projectile> Projectiles;

    public bool hasStoredProjectile => Projectiles.Count > 0;

    public Projectile GetProjectile(Vector2 pos){
      
        Projectile toBeShooted; 
                  
        if(hasStoredProjectile){
            toBeShooted = Projectiles[0];
            toBeShooted.transform.position = pos;
            Projectiles.Remove(toBeShooted);
            toBeShooted.gameObject.SetActive(true);
        } 
        else {
            toBeShooted = Instantiate(ProjectilePrefab, pos, Quaternion.identity).GetComponent<Projectile>();
            toBeShooted.transform.SetParent(transform);
        }
        
        return toBeShooted;
    }
}
