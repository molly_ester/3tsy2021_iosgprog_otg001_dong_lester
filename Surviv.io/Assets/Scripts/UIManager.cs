﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIManager : Singleton<UIManager> {

    public List<UICanvas> Canvases;
    UICanvas curActive;

    public void SetActive(int x){
        curActive?.Hide();
        Canvases[x].Show();
        curActive = Canvases[x];
    }
}
