﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainMenu : UICanvas {
    
    [SerializeField] Image[] rend;
    [SerializeField] Sprite[] mainMenuBg;

    int curBg = 0;
    int curRend = 0;

    protected override void Start(){ 
        //rend[curRend].gameObject.SetActive(true);
        base.Start();
        GetComponentInChildren<Button>().onClick.AddListener(() => GameManager.Instance.SetState(GameState.Game));
    }

    public override void Show(){        
        rend[curRend].overrideSprite = RandomizeBackground();
        rend[curRend].color = new Vector4(1,1,1,1);
        base.Show();
        StartCoroutine(ChangeBackground());
    }

    IEnumerator ChangeBackground(){
        yield return new WaitForSeconds(5);

        int oldRendNum = curRend;
        curRend = curRend + 1 >= rend.Length ? 0 : 1;
        rend[curRend].overrideSprite = RandomizeBackground();
        rend[curRend].color = new Vector4(1,1,1,0);
        rend[curRend].gameObject.SetActive(true);


        Vector4 color1 = new Vector4(1, 1, 1, 0);
        Vector4 color2 = new Vector4(1, 1, 1, 1);
        while(rend[oldRendNum].color.a > 0 || rend[curRend].color.a < 1){
        
            color1.w += Time.deltaTime;
            color2.w -= Time.deltaTime;

            rend[oldRendNum].color = color2;
            rend[curRend].color = color1;
            yield return null;
        }
        rend[oldRendNum].gameObject.SetActive(false);
        
        StartCoroutine(ChangeBackground());
    }

    Sprite RandomizeBackground(){
        int rand;
        do {
           rand = UnityEngine.Random.Range(0, mainMenuBg.Length);
        } while (rand == curBg);
        curBg = rand;
        return mainMenuBg[curBg];
    }
}
