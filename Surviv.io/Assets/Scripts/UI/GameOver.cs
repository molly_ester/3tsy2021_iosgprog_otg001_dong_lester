﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : UICanvas {
    
    [SerializeField] Text rankText;
    [SerializeField] Button MainMenu;
    [SerializeField] Button Retry;

    protected override void Start(){
        base.Start();
        MainMenu.onClick.AddListener(() => GameManager.Instance.SetState(GameState.MainMenu));
        Retry.onClick.AddListener(() => GameManager.Instance.SetState(GameState.Game));
    }

    public override void Show(){
        base.Show();
        rankText.text = "Rank #" + GameManager.Instance.numOfAliveUnits.ToString();
        
    }
}
