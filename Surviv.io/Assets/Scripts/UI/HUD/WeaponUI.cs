﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class WeaponUI : MonoBehaviour {

    [SerializeField] Image image;
    Button button;

    void Awake(){ 
        button = GetComponent<Button>();
    }

    public void UpdateSprite(WeaponHandler handler) {
        image.overrideSprite = handler.GetNextGun()?.Sprite ?? null;
    }

    public void AddListener(UnityAction act){
        button.onClick.AddListener(act);
    }

    public void Refresh() {
        image.overrideSprite = null;
    }
}
 