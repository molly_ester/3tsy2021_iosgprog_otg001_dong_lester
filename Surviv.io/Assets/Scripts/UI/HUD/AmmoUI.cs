﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUI : MonoBehaviour{
    
    [SerializeField] Text ammo;
    [SerializeField] Text clipCap;

    public void UpdateAmmo(WeaponHandler handler){
        ammo.text = handler.CurMagazine().Amount.ToString();
    }

    public void UpdateClip(WeaponHandler handler){
        clipCap.text = handler.CurEquippedGun != null ? handler.CurEquippedGun.Ammo.ToString() : "0";
    }

    public void Refresh(){
        ammo.text = "0";
        clipCap.text = "0";
    }
}
