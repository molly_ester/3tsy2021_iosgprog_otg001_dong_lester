﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ShootButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Image image;
    public UnityEvent EvtOnClick;
    public UnityEvent EvtOnClickHold;

    bool onHold;

    void Start(){
        image = GetComponent<Image>();
    }
    // Start is called before the first frame update
    public virtual void OnPointerDown(PointerEventData evtData){
        EvtOnClick.Invoke();
        onHold = true;
        image.color = new Vector4(1,1,1,.5f);
    }

    public virtual void OnPointerUp(PointerEventData evtData){
        onHold = false;
         image.color = new Vector4(1,1,1,1);
    }

    void Update(){
        if(onHold){
            EvtOnClickHold.Invoke();
        }
    }
}
