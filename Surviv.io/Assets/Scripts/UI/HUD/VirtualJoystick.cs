﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler  {

    [SerializeField] Image outerCircle;
    [SerializeField] Image innerCircle;
    Vector2 inputVector;

    public Vector2 InputVector => inputVector;

    public virtual void OnPointerDown(PointerEventData evtData){
        OnDrag(evtData);
    }

    public virtual void OnPointerUp(PointerEventData evtData){
        innerCircle.rectTransform.anchoredPosition = Vector2.zero;
        inputVector = Vector2.zero;
    }

    public virtual void OnDrag(PointerEventData evtData){
        Vector2 pos;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(outerCircle.rectTransform, evtData.position, evtData.pressEventCamera, out pos)){
            pos.x = (pos.x/outerCircle.rectTransform.sizeDelta.x);
            pos.y = (pos.y/outerCircle.rectTransform.sizeDelta.y);
            inputVector = new Vector2 (pos.x * 2, pos.y * 2);
            inputVector = inputVector.magnitude > 1 ? inputVector.normalized : inputVector;
            innerCircle.rectTransform.anchoredPosition = inputVector * outerCircle.rectTransform.sizeDelta/3f;
        }
    }
}
