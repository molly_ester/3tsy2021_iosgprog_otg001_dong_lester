﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagazineUI : MonoBehaviour {
    
    [SerializeField]Text text;
    [SerializeField] AmmoType ammoType;

    public void UpdateMagazine(WeaponHandler handler){
        text.text = handler.Ammos[(int)ammoType].Amount.ToString();
    }

    public void Refresh(){
        text.text = "0";
    }
}
