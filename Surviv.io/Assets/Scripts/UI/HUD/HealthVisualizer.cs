﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthVisualizer : MonoBehaviour {

    [SerializeField] Image image;

    public IEnumerator UpdateFill(Health health){
        while (image.fillAmount != health.Ratio) {
            image.fillAmount = Mathf.Lerp(image.fillAmount, health.Ratio, Time.deltaTime );

            if(image.fillAmount <= .30f) image.color = Color.red;
            else image.color = Color.white;
                
            yield return null;
        }
    }

    public void RefreshFill(){
        image.fillAmount = 1;
        image.color = Color.white;
    }
}
