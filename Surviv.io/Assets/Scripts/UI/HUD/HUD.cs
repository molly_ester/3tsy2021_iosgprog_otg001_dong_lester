﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HUD: UICanvas {

    Player player;
    
    HealthVisualizer healthUI;
    AmmoUI ammoUI;
    WeaponUI weaponUI;
    MagazineUI[] magazineUI = new MagazineUI[3];
    
    [SerializeField] VirtualJoystick moveJoystick;
    [SerializeField] VirtualJoystick aimJoyStick;
    [SerializeField] ShootButton shootButton;

    protected override void Start(){
        base.Start();
        healthUI = GetComponentInChildren<HealthVisualizer>();
        ammoUI = GetComponentInChildren<AmmoUI>();
        weaponUI = GetComponentInChildren<WeaponUI>();
        magazineUI = GetComponentsInChildren<MagazineUI>();
        Refresh();

        GameManager.Instance.EvtGameStart += InitializePlayer;
        GameManager.Instance.EvtGameEnd += () => {
            player = null;
            Refresh();
        };

        /*
        player.WeaponHandler.EvtAmmoChange.AddListener((handler) => {
            ammoUI.UpdateAmmo(handler);
            ammoUI.UpdateClip(handler);
            foreach(MagazineUI mag in magazineUI){
                mag.UpdateMagazine(handler);
            }
        });

        player.WeaponHandler.EvtNewWeapon.AddListener(weaponUI.UpdateSprite);
        
        weaponUI.AddListener(() => {
            if(player.WeaponHandler.HasNextGun()){
                player.WeaponHandler.NextGun();
                weaponUI.UpdateSprite(player.WeaponHandler);
            }
        });

        player.Health.EvtChangeHealth.AddListener((health) => StartCoroutine(healthUI.UpdateFill(health)));
        
        player.moveJoystick = moveJoystick;
        player.aimJoyStick = aimJoyStick;
        player.shootButton = shootButton;

        shootButton.EvtOnClick.AddListener(player.WeaponHandler.Attack);
        shootButton.EvtOnClickHold.AddListener(player.WeaponHandler.HoldableAttack);
        */
    }

    void InitializePlayer(){
        player = GameManager.Instance.Player;
        player.WeaponHandler.EvtAmmoChange.AddListener((handler) => {
            ammoUI.UpdateAmmo(handler);
            ammoUI.UpdateClip(handler);
            foreach(MagazineUI mag in magazineUI){
                mag.UpdateMagazine(handler);
            }
        });

        player.WeaponHandler.EvtNewWeapon.AddListener(weaponUI.UpdateSprite);
        
        weaponUI.AddListener(() => {
            if(player.WeaponHandler.HasNextGun()){
                player.WeaponHandler.NextGun();
                weaponUI.UpdateSprite(player.WeaponHandler);
            }
        });

        player.Health.EvtChangeHealth.AddListener((health) => StartCoroutine(healthUI.UpdateFill(health)));
        
        player.moveJoystick = moveJoystick;
        player.aimJoyStick = aimJoyStick;
        player.shootButton = shootButton;

        shootButton.EvtOnClick.AddListener(player.WeaponHandler.Attack);
        shootButton.EvtOnClickHold.AddListener(player.WeaponHandler.HoldableAttack);
    }

    void Refresh(){
        healthUI.RefreshFill();
        ammoUI.Refresh();
        weaponUI.Refresh();
        foreach(MagazineUI mag in magazineUI){
            mag.Refresh();
        }
    }
}
