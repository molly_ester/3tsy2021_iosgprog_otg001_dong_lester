﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State {
    
    protected EnemyBehaviour behaviour;

    public State(EnemyBehaviour behaviour){
        this.behaviour = behaviour;
    }
    public abstract IEnumerator Start();


}
