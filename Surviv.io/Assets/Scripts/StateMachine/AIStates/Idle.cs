﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : State {

    public Idle(EnemyBehaviour behaviour) : base(behaviour){

    }

    public override IEnumerator Start() {
        yield return new WaitForSeconds(Random.Range(.5f, 1.5f));

        behaviour.SetState(new Patrol(behaviour));
    }
}
