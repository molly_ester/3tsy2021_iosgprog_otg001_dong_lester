﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looting : State {
    List<Container> containersOnRange;
    WeaponHandler handler;
    Vector2 targetContainerPos;

    public override IEnumerator Start() {
        Movement movement = behaviour.Enemy.Movement;

        while(CheckLoot() != null){
            targetContainerPos = CheckLoot().transform.position;
            Transform enemyTransform = behaviour.Enemy.transform;
            Vector2 dir = (targetContainerPos - (Vector2) enemyTransform.position).normalized;
            float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            enemyTransform.eulerAngles = new Vector3(0, 0, rotation);
            
            while (!movement.MoveToPosition(targetContainerPos)){
                yield return null;
            }
        }
        
        State newState = behaviour.Enemy.OnEnemyRange.Count > 0 && handler.CurEquippedGun ? new Combat(behaviour) as State : new Idle(behaviour) as State;
        behaviour.SetState(newState);
    }

    public Looting(EnemyBehaviour behaviour) : base(behaviour) {
        containersOnRange = behaviour.Enemy.OnContainerRange;
        handler = behaviour.Enemy.WeaponHandler;
    }

    Container CheckLoot(){
        Container toRemove = null;
        for(int i = 0; i < containersOnRange.Count; i++){
            if(containersOnRange[i].Loot() is Gun){
                Gun lootableGun = containersOnRange[i].Loot() as Gun;
                if((lootableGun is AutomaticGun || lootableGun is Shotgun) && !handler.HasPrimaryGun){
                    return containersOnRange[i];
                }
                else if (lootableGun is Pistol && !handler.HasSecondaryGun){
                    return containersOnRange[i];
                }
                else {
                    toRemove = containersOnRange[i];
                }
            } else if(containersOnRange[i].Loot() is Ammo){
                return containersOnRange[i];
            }
        }
        
        if(toRemove){
            containersOnRange.Remove(toRemove);
        }
        return null;
    }
}
