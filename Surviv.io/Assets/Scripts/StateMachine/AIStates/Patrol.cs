﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : State {
    
    
    Vector2 targetPos;

    public override IEnumerator Start() {
        Movement movement = behaviour.Enemy.Movement;
        Wander();
        Transform enemyTransform = behaviour.Enemy.transform;
        Vector2 dir = (targetPos - (Vector2) enemyTransform.position).normalized;
        float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        enemyTransform.eulerAngles = new Vector3(0, 0, rotation);

        while(!movement.MoveToPosition(targetPos)){
            yield return null;
        }

        behaviour.SetState(new Idle(behaviour));
    } 

    void Wander(){
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        Vector2 wanderTarget = new Vector2(Random.Range(-1f, 1f) * wanderJitter, Random.Range(-1f, 1f) * wanderJitter);
        wanderTarget = wanderTarget.normalized;

        wanderTarget *= wanderRadius;

        Vector2 targetLocal = wanderTarget + new Vector2(wanderDistance, 0);
        Vector2 targetWorld = behaviour.transform.InverseTransformVector(targetLocal);

        targetPos = targetWorld;
    }

    public Patrol(EnemyBehaviour behaviour) : base(behaviour) {

    }
}
