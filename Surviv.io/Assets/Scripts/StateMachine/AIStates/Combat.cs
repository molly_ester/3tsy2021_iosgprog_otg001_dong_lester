﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : State {
    List<Unit> EnemyOnRange;
    WeaponHandler handler;
    Unit target;

    
    
    public Combat(EnemyBehaviour behaviour) : base(behaviour){
        handler = behaviour.Enemy.WeaponHandler;
        EnemyOnRange = behaviour.Enemy.OnEnemyRange;
        
    }

    public override IEnumerator Start() {
        while((target = FindLowestHealth()) && HasShootableGun()){
            handler.Direction = (target.transform.position - behaviour.transform.position).normalized;
            float rot = Mathf.Atan2(handler.Direction.y, handler.Direction.x) * Mathf.Rad2Deg;
            behaviour.transform.eulerAngles = new Vector3(0, 0, rot);
                /*while(behaviour.transform.eulerAngles.z != rot){
                    float newRot = Mathf.Lerp(behaviour.transform.eulerAngles.z, rot, Time.deltaTime);
                    
                    yield return null;
                }*/
                
            handler.Attack();
            yield return null;
        }
        State newState = behaviour.Enemy.OnContainerRange.Count > 0 ? new Looting(behaviour) as State : new Idle(behaviour) as State;
        behaviour.SetState(newState);
    }

    Unit FindLowestHealth(){
        if(EnemyOnRange.Count == 0) return null;
        int index = 0;
        Unit toReturn = EnemyOnRange[0];
        for(int i = 1; i < EnemyOnRange.Count; i++){
            if(toReturn.Health.CurHealth > EnemyOnRange[i].Health.CurHealth){
                index = i;
                toReturn = EnemyOnRange[i];
            }
        }

        return toReturn.Health.IsDead ? null : toReturn;
    }

    bool HasShootableGun(){
        if(!handler.CurEquippedGun && !handler.HasNextGun()) 
            return false;

        if(handler.CurEquippedGun.hasAmmo || handler.CanReload()) 
            return true;

        if(!handler.CurEquippedGun.hasAmmo && !handler.CanReload()){
            if(!handler.HasNextGun()) 
                return false;
            
            if(handler.GetNextGun().hasAmmo || handler.CanReload(handler.GetNextGun())){
                handler.NextGun();
                return true;
            }
        }

        return false;
    }
}
