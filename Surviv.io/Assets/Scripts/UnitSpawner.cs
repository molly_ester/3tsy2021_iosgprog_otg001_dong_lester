﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawner : MonoBehaviour {

    [SerializeField] Player playerPrefab;
    [SerializeField] Unit AIPrefab;
    [SerializeField] Gun[] guns;
    [SerializeField] Ammo[] ammos;

    public Player SpawnPlayer(){
        return Instantiate(playerPrefab, new Vector3(Random.Range(-90f, 90f), Random.Range(-90f, 90f), 0), Quaternion.identity);
    }

    public Unit SpawnUnit(){
        Unit unit = Instantiate(AIPrefab, new Vector3(Random.Range(-90f, 90f), Random.Range(-90f, 90f), 0), Quaternion.identity);
        WeaponHandler handler = unit.WeaponHandler;
        int random = Random.Range(0, 3);

        handler.AddGun(Instantiate(guns[random]));

        Ammo ammo = Instantiate(ammos[random]);
        ammo.AddAmmo(Random.Range(10, 15 + 1));

        handler.AddAmmo(ammo);

        return unit;
    }


}
