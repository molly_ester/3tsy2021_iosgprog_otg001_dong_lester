﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public int Damage;

    void Start(){
        GameManager.Instance.EvtGameEnd += () => gameObject.SetActive(false);
    }


    void OnEnable() {
        transform.SetParent(PoolingManager.Instance.transform);
        StartCoroutine(HideProjectile());
    }

    IEnumerator HideProjectile(){
        yield return new WaitForSeconds(5f);

        gameObject.SetActive(false);
    }

    public void Initialize(Vector2 velocity) {
        GetComponent<Rigidbody2D>().velocity = velocity;
    }

    void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("Unit") || other.gameObject.CompareTag("Obstacle")){
            other.gameObject.GetComponent<Health>()?.TakeDamage(Damage);
            gameObject.SetActive(false);
        } 
    }

    void OnDisable() {
        PoolingManager.Instance?.Projectiles.Add(this);
    }
}
