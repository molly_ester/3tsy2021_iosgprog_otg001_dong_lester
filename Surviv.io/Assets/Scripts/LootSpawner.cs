﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawningType{
    SmartSpawning,
    NormalSpawning
}
public class LootSpawner : MonoBehaviour {
    
    [SerializeField] Container prefab;
    [SerializeField] Gun[] guns;
    [SerializeField] Ammo[] ammos;
    [SerializeField] SpawningType spawnStyle;

    public Container SpawnGun(Vector2 position){
        Container newCont = Spawn(position);
        newCont.InitializeContainer(Instantiate(guns[Random.Range(0, guns.Length)]));
        return newCont;
    }

    public Container SpawnGun(Vector2 position, int index){
        Container newCont = Spawn(position);
        newCont.InitializeContainer(Instantiate(guns[index]));
        return newCont;
    }

    public Container SpawnAmmo(Vector2 position){
        Container newCont = Spawn(position);
        Ammo ammo = Instantiate(ammos[Random.Range(0, ammos.Length)]);
        ammo.AddAmmo(Random.Range(10, 20));

        newCont.InitializeContainer(ammo);
        return newCont;
    }

    public Container SpawnAmmo(Vector2 position, int index){
        Container newCont = Spawn(position);
        Ammo ammo = Instantiate(ammos[index]);
        ammo.AddAmmo(Random.Range(10, 20));

        newCont.InitializeContainer(ammo);
        return newCont;
    }

    Container Spawn(Vector2 position){
        return Instantiate(prefab, position, Quaternion.identity);
    }

    public List<Container> ContainerSpawns(){
        List<Container> containers = new List<Container>();
        switch(spawnStyle){
            case SpawningType.SmartSpawning:
                for(int i = 0; i < guns.Length; i++){
                    for(int j = 0; j < (i + 1)* 5; j++){
                        Vector2 posSpawn = new Vector2(Random.Range(-80, 80f), Random.Range(-80f, 80f));
                        Container gun = SpawnGun(posSpawn, i);
                        containers.Add(gun);
                        gun.EvtTaken.AddListener((cont) => containers.Remove(cont));
                        
                        int numberOfAmmo = Random.Range(1,4);
                        for(int k = 0; k < numberOfAmmo; k++){
                            posSpawn += new Vector2(Random.Range(-2f, 3f), Random.Range(-2f, 3f));
                            Container ammo = SpawnAmmo(posSpawn, i);
                            containers.Add(ammo);
                            ammo.EvtTaken.AddListener((cont) => containers.Remove(cont));
                        }
                    }
                }


            break;
            case SpawningType.NormalSpawning:
                for(int i = 0; i < 20; i++){
                    Container gun = SpawnGun(new Vector2(Random.Range(-50f, 50f), Random.Range(-50f, 50f)));
                    Container ammo = SpawnAmmo(new Vector2(Random.Range(-50f, 50f), Random.Range(-50f, 50f)));
                    containers.Add(gun);
                    containers.Add(ammo);
                    gun.EvtTaken.AddListener((cont) => containers.Remove(cont));
                    ammo.EvtTaken.AddListener((cont) => containers.Remove(cont));
                }
            break;  
        } 

        return containers;
    }
}
