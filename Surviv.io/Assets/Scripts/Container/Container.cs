﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ContainerEvt : UnityEvent<Container> { }
public class Container : MonoBehaviour {
    
    [SerializeField] SpriteRenderer bgRend;
    [SerializeField] SpriteRenderer spriteRend;
    ScriptableObject contained;
    public bool IsTaken;
    public ContainerEvt EvtTaken {get; private set;}
    
    void Awake(){
        EvtTaken = new ContainerEvt();
    }
    public ScriptableObject Loot(){
        return contained;
    }

    public void InitializeContainer(ScriptableObject obj){
        contained = obj;
        IGunType init = obj as IGunType;

        spriteRend.sprite = init.Sprite;
        
        switch (init.AmmoType){
            case AmmoType.pistol:
                bgRend.color = new Vector4(210/255f, 105/255f, 30/255f, 1);
                break;
            case AmmoType.auto:
                bgRend.color = Color.green;
                break;
            case AmmoType.shotgun:
                bgRend.color = Color.red; 
                break;
        }
    }

    void OnDestroy() {
        if(IsTaken)
            EvtTaken.Invoke(this);    
        
        else 
            Destroy(contained);
    }
}
