﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState{
    MainMenu, Game, GameOver
}

public class GameManager : Singleton<GameManager> {

    protected GameManager() { }
    [SerializeField] LootSpawner lootSpawner;
    [SerializeField] UnitSpawner unitSpawner;
    public Player Player;
    List<Unit> enemies = new List<Unit>();
    List<Container> SpawnedCont;

    public int numOfAliveUnits;

    GameState gameState;

    public Action EvtGameStart;
    public Action EvtGameEnd;

    void Start(){
        StartCoroutine(LoadScene("GameScene"));
        StartCoroutine(LoadScene("UI",  () => UIManager.Instance.SetActive(0)));
        
        EvtGameStart += () => {
            numOfAliveUnits = UnityEngine.  Random.Range(10, 20 + 1);
            SpawnedCont = lootSpawner.ContainerSpawns();
            
            //if(Player) Destroy(Player);
            Player = unitSpawner.SpawnPlayer();
            Player.Health.EvtDeath.AddListener((health) => SetState(GameState.GameOver));
            
            for(int i = 0; i < numOfAliveUnits - 1; i++){
                enemies.Add(unitSpawner.SpawnUnit());
                int x = i;
                enemies[x].Health.EvtDeath.AddListener((health) => {
                    numOfAliveUnits -= 1;
                    enemies[x].gameObject.SetActive(false);
                    if(numOfAliveUnits == 1){
                        SetState(GameState.GameOver);
                    }
                });
            }
        };

        EvtGameEnd += () => {

            for(int i = SpawnedCont.Count - 1; i >= 0; i--){
                Container cont = SpawnedCont[i];
                SpawnedCont.Remove(cont);
                Destroy(cont.gameObject);
            }
            
            Destroy(Player.gameObject);

            for(int i = enemies.Count - 1; i >= 0; i--){
                Unit unit = enemies[i];
                enemies.Remove(unit);
                Destroy(unit.gameObject);
            }
        };
    }
    IEnumerator LoadScene(string sceneName, Action onCallBack = null){
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        
        while(!asyncLoad.isDone){
            yield return null;
        }

        onCallBack?.Invoke();
    }

    public void SetState(GameState state){
        gameState = state;

        switch(state){
            case GameState.MainMenu:
                UIManager.Instance.SetActive(0);
                
                break;
            case GameState.Game:
                UIManager.Instance.SetActive(1);
                EvtGameStart.Invoke();
                break;
            case GameState.GameOver:
                UIManager.Instance.SetActive(2);
                EvtGameEnd.Invoke();
                break;
        }
    }

}
