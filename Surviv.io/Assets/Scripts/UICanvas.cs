﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvas : MonoBehaviour
{

    protected virtual void Start(){
        UIManager.Instance.Canvases.Add(this);
        Hide();
    }

    public virtual void Show(){
        gameObject.SetActive(true);
    }

    public virtual void Hide(){
        gameObject.SetActive(false);
    }
}
